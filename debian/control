Source: hepmc
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Lifeng Sun <lifongsun@gmail.com>
Build-Depends: debhelper (>= 11~),
Build-Depends-Indep: doxygen-latex, ghostscript, texlive, texlive-science
Standards-Version: 4.2.0
Homepage: http://hepmc.web.cern.ch/hepmc/
Vcs-Git: https://salsa.debian.org/science-team/hepmc.git
Vcs-Browser: https://salsa.debian.org/science-team/hepmc
# Upstream-Git: https://gitlab.cern.ch/hepmc/HepMC

Package: libhepmc4
Section: libs
Architecture: any
Multi-arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: libhepmcfio
Suggests: libhepmc-dev, libhepmcfio-dev
Description: Event Record for Monte Carlo Generators
 The HepMC package is an object oriented event record written in C++ for
 High Energy Physics Monte Carlo Generators.
 .
 Many extensions from HEPEVT, the Fortran HEP standard, are supported: the
 number of entries is unlimited, spin density matrices can be stored with
 each vertex, flow patterns (such as color) can be stored and traced,
 integers representing random number generator states can be stored, and an
 arbitrary number of event weights can be included. Particles and vertices
 are kept separate in a graph structure, physically similar to a physics
 event.
 .
 The added information supports the modularisation of event generators.
 Event information is accessed by means of iterators supplied with the
 package. 
 .
 This package ships the shared object for HepMC.

Package: libhepmc-dev
Section: libdevel
Architecture: any
Depends: libhepmc4 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Recommends: libhepmcfio-dev
Suggests: hepmc-examples, hepmc-user-manual, hepmc-reference-manual
Description: Event Record for Monte Carlo Generators - development files
 The HepMC package is an object oriented event record written in C++ for
 High Energy Physics Monte Carlo Generators.
 .
 Many extensions from HEPEVT, the Fortran HEP standard, are supported: the
 number of entries is unlimited, spin density matrices can be stored with
 each vertex, flow patterns (such as color) can be stored and traced,
 integers representing random number generator states can be stored, and an
 arbitrary number of event weights can be included. Particles and vertices
 are kept separate in a graph structure, physically similar to a physics
 event.
 .
 The added information supports the modularisation of event generators.
 Event information is accessed by means of iterators supplied with the
 package.
 .
 This package provides development files for HepMC.

Package: libhepmcfio4
Section: libs
Architecture: any
Multi-arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libhepmc4 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Suggests: libhepmcfio-dev, libhepmc-dev
Description: Monte Carlo event record FIO library
 The HepMC package is an object oriented event record written in C++ for
 High Energy Physics Monte Carlo Generators.
 .
 Many extensions from HEPEVT, the Fortran HEP standard, are supported: the
 number of entries is unlimited, spin density matrices can be stored with
 each vertex, flow patterns (such as color) can be stored and traced,
 integers representing random number generator states can be stored, and an
 arbitrary number of event weights can be included. Particles and vertices
 are kept separate in a graph structure, physically similar to a physics
 event.
 .
 The added information supports the modularisation of event generators.
 Event information is accessed by means of iterators supplied with the
 package.
 .
 The fio library of HepMC provides wrappers of HEPEVT, Pythia and Herwig.

Package: libhepmcfio-dev
Section: libdevel
Architecture: any
Depends: libhepmc-dev (= ${binary:Version}), libhepmcfio4 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Suggests: hepmc-example, hepmc-user-manual, hepmc-reference-manual
Description: Monte Carlo event record FIO library - development files
 The HepMC package is an object oriented event record written in C++ for
 High Energy Physics Monte Carlo Generators.
 .
 Many extensions from HEPEVT, the Fortran HEP standard, are supported: the
 number of entries is unlimited, spin density matrices can be stored with
 each vertex, flow patterns (such as color) can be stored and traced,
 integers representing random number generator states can be stored, and an
 arbitrary number of event weights can be included. Particles and vertices
 are kept separate in a graph structure, physically similar to a physics
 event.
 .
 The added information supports the modularisation of event generators.
 Event information is accessed by means of iterators supplied with the
 package.
 .
 This package provides development files for fio library of HepMC.

Package: hepmc-examples
Section: devel
Architecture: all
Depends: libclhep-dev, libpythia8-dev, libhepmc-dev (>= ${source:Version}), libhepmcfio-dev (>= ${source:Version}), ${misc:Depends}
Suggests: hepmc-user-manual, hepmc-reference-manual
Description: Event Record for Monte Carlo Generators - example files
 The HepMC package is an object oriented event record written in C++ for
 High Energy Physics Monte Carlo Generators.
 .
 Many extensions from HEPEVT, the Fortran HEP standard, are supported: the
 number of entries is unlimited, spin density matrices can be stored with
 each vertex, flow patterns (such as color) can be stored and traced,
 integers representing random number generator states can be stored, and an
 arbitrary number of event weights can be included. Particles and vertices
 are kept separate in a graph structure, physically similar to a physics
 event.
 .
 The added information supports the modularisation of event generators.
 Event information is accessed by means of iterators supplied with the
 package.
 .
 This package provides example source files for HepMC.

Package: hepmc-user-manual
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: libhepmc-dev, libhepmcfio-dev
Suggests: hepmc-reference-manual
Description: Event Record for Monte Carlo Generators - user manual
 The HepMC package is an object oriented event record written in C++ for
 High Energy Physics Monte Carlo Generators.
 .
 Many extensions from HEPEVT, the Fortran HEP standard, are supported: the
 number of entries is unlimited, spin density matrices can be stored with
 each vertex, flow patterns (such as color) can be stored and traced,
 integers representing random number generator states can be stored, and an
 arbitrary number of event weights can be included. Particles and vertices
 are kept separate in a graph structure, physically similar to a physics
 event.
 .
 The added information supports the modularisation of event generators.
 Event information is accessed by means of iterators supplied with the
 package.
 .
 This package provides user manual for HepMC2.

Package: hepmc-reference-manual
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: libhepmc-dev, libhepmcfio-dev, hepmc-user-manual
Description: Event Record for Monte Carlo Generators - reference manual
 The HepMC package is an object oriented event record written in C++ for
 High Energy Physics Monte Carlo Generators.
 .
 Many extensions from HEPEVT, the Fortran HEP standard, are supported: the
 number of entries is unlimited, spin density matrices can be stored with
 each vertex, flow patterns (such as color) can be stored and traced,
 integers representing random number generator states can be stored, and an
 arbitrary number of event weights can be included. Particles and vertices
 are kept separate in a graph structure, physically similar to a physics
 event.
 .
 The added information supports the modularisation of event generators.
 Event information is accessed by means of iterators supplied with the
 package.
 .
 This package provides reference manual for HepMC2.
