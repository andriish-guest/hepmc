hepmc (2.06.09-3) unstable; urgency=medium

  * Team Upload.
  * Cherry-pick from upstream-git: enlarge tolerance of floating point error.
    Thanks to Adrian Bunk. (Closes: #906708) (https://gcc.gnu.org/PR87036)
  * Update Homepage to http://hepmc.web.cern.ch/hepmc/ (Closes: #906709)
  * Update package descriptions. (Closes: #688671)
  * Bump Standards-Version to 4.2.0 .

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 23 Aug 2018 06:58:13 +0000

hepmc (2.06.09-2) unstable; urgency=medium

  * Team Upload.
  * Replace B-D-I:texlive-math-extra with texlive,texlive-science.
    The original one no longer exists in archive. (Closes: #867090)
  * Upgrade from Debhelper compat level 7 to level 11.
  * Overhaul debian/rules in debhelper 11 style.
  * Patch src/IO_AsciiParticles.cc to amend floating point format.
    The FTBFS is due to floatpoint printing format mismatch during test,
    triggered by GCC-5's change. (+ gcc-5plus.patch) (Closes: #777901)
  * Deprecate DM-Upload-Allowed field.
  * Strip useless build-dependencies.
  * Annotate Upstream-Git URI in control.
  * Point Vcs-* fields to Salsa due to Alioth deprecation.

 -- Mo Zhou <cdluminate@gmail.com>  Wed, 15 Aug 2018 02:43:16 +0000

hepmc (2.06.09-1) unstable; urgency=low

  * New upstream.
  * debian/control: move doxygen-latex, ghostscript, texlive-math-extra to
    build-dep-indep.
  * debian/copyright: fix licence version.
  * debian/rules: support get-orig-source rule.

 -- Lifeng Sun <lifongsun@gmail.com>  Thu, 21 Jun 2012 19:59:00 +0800

hepmc (2.06.08-1) unstable; urgency=low

  * Initial release (Closes: #636976)

 -- Lifeng Sun <lifongsun@gmail.com>  Sun, 20 May 2012 14:26:51 +0800
